<?php
/**
 * require folder with same name as this file. Folder must contains file local.ini and .htaccess for target site
 */

/** @var array $commonSettings */
$commonSettings = parse_ini_file('deployment.ini');

if (isset($commonSettings['after'])) {
    array_walk($commonSettings['after'], static function (&$item) {
        $item = str_replace('URL_SETUP', 'http://dev.kobyla.name/test-ci-cd/setup/setup.php?production', $item);
    });
}

return array_merge(
    $commonSettings,
    [
        'remote' => getenv('FTP_STAGE_URL'),
        'local' => dirname(__DIR__) . '/www',
    ]
);
